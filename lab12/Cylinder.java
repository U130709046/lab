package midterm;

public class Cylinder {//question9
	private Circle c=null;
	private Rectangle r=null;
	
	public Cylinder(double radius, double height) {
		c=new Circle(radius);//saklamak i�in yeni bir circle olu�turuyoruz
        r= new Rectangle(c.perimeter(),height);
	}
	
	public double area() {
		return 2*c.area()+r.area();
		
	}
	public double volume() {
		return c.area()*r.getLength();
	}

}
